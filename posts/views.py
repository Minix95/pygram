from django.shortcuts import render
from django.views.generic.list import ListView
from posts.models import Posts

class PostList(ListView):
    model = Posts
